const express = require("express");
const app = express();
const PORT = 3000;

app.use(express.json());
app.use("/", require("./routes/index"));

app.listen(PORT, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port 3000");
});
