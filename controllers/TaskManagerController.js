module.exports.getAllTasks = (req, res) => {
    return res.send("All Tasks From the file");
};

module.exports.createTask = (req, res) => {
    return res.send("Create tasks");
};

module.exports.getTask = (req, res) => {
    return res.send("Get tasks");
};

module.exports.updateTask = (req, res) => {
    return res.send("Update tasks");
};

module.exports.deleteTask = (req, res) => {
    return res.send("Delete tasks");
};
